# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# This file is used to configure which externals should be picked up from
# external (AFS/CVMFS) sources.
#

# Find LCG:
set( LCG_VERSION_POSTFIX @LCG_VERSION_POSTFIX@ )
if( AthAnalysisExternals_FIND_QUIETLY )
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT QUIET )
else()
   find_package( LCG @LCG_VERSION_NUMBER@ REQUIRED EXACT )
endif()

# Set the platform name for Gaudi, and the externals:
set( ATLAS_PLATFORM @ATLAS_PLATFORM@ )
set( CMTCONFIG @ATLAS_PLATFORM@ )
set( ENV{CMTCONFIG} ${CMTCONFIG} )

# Add all custom compilation options:
set( CMAKE_CXX_STANDARD 14 CACHE STRING "C++ standard used for the build" )
add_definitions( -DGAUDI_V20_COMPAT )
add_definitions( -DATLAS_GAUDI_V21 )
add_definitions( -DHAVE_GAUDI_PLUGINSVC )

# Allow non-constness for ToolHandles. This is for release 22
add_definitions( -DALLOW_TOOLHANDLE_NONCONSTNESS )

# compilation option for AthenaMT
add_definitions( -DATHENAHIVE )

# Forcefully turn off the HDF5_ROOT setting coming from AtlasLCG. Since CMake's
# FindHDF5.cmake module is unable to find HDF5 inside this project if that
# variable is set to anything (other than this project's directory).
set( HDF5_ROOT "" CACHE PATH "Path to find HDF5 under" FORCE )

# Use the -pthread flag for the build instead of the -lpthread linker option,
# whenever possible:
set( THREADS_PREFER_PTHREAD_FLAG TRUE CACHE BOOL
   "Prefer using the -pthread compiler flag over -lpthread" )
