# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building KLFitter as part of an analysis release
#

# The name of the package:
atlas_subdir( KLFitter )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# In C++17 mode BAT is not built. So let's turn off the build of KLFitter
# as well.
if( "${CMAKE_CXX_STANDARD}" GREATER 14 )
   message( WARNING "Turning off the build of KLFitter for C++${CMAKE_CXX_STANDARD}" )
   return()
endif()

# Set ROOTSYS based on the build environment:
if( ATLAS_BUILD_ROOT )
   set( _rootsys ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( ROOT REQUIRED )
   set( _rootsys ${ROOTSYS} )
endif()

# Set the root directory of the ATLAS-built BAT version and
# forward it to the KLFitter cmake config.
set( _bat_root ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )

# KLFitter version and hash
set( _version_tag "v1.2.1" )
set( _version_hash "b7394ec81461cf8d88ed518d10a9fda9" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/KLFitterBuild )

# Build KLFitter for the build area:
ExternalProject_Add( KLFitter
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL http://cern.ch/atlas-software-dist-eos/externals/KLFitter/${_version_tag}.tar.gz
   URL_MD5 ${_version_hash}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DROOT_ROOT:PATH=${_rootsys}
   -DBAT_ROOT:PATH=${_bat_root}
   -DINSTALL_EXAMPLES:BOOL=FALSE
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( KLFitter forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of KLFitter"
   DEPENDERS download )
ExternalProject_Add_Step( KLFitter purgeBuild
  COMMAND ${CMAKE_COMMAND} -E remove_directory
  "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/include/KLFitter"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
  COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
  COMMENT "Removing previous build results for KLFitter"
  DEPENDEES download
  DEPENDERS configure )
ExternalProject_Add_Step( KLFitter forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of KLFitter"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( KLFitter buildinstall
  COMMAND ${CMAKE_COMMAND} -E remove_directory
   ${_buildDir}/cmake
  COMMAND ${CMAKE_COMMAND} -E copy_directory
  ${_buildDir}/ <INSTALL_DIR>
  COMMENT "Installing KLFitter into the build area"
  DEPENDEES install )
add_dependencies( Package_KLFitter KLFitter )
add_dependencies( KLFitter BAT )
if( ATLAS_BUILD_ROOT )
   add_dependencies( KLFitter ROOT )
endif()

# Install KLFitter:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES cmake/FindKLFitter.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
