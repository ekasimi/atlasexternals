Coin3D, Simage
==============

This package builds the Coin3D-simage library for the offline software. To be
picked up by the event visualisation code.

The code is included in the repository itself, since I didn't find a good place
to download it from. But later on we may want to do the build like that
instead.
