# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Defines:
#
#  TBB_FOUND
#  TBB_INCLUDE_DIR
#  TBB_INCLUDE_DIRS
#  TBB_LIBRARIES
#  TBB_LIBRARY_DIRS
#  TBB_VERSION
#
# Can be steered using TBB_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME TBB
   INCLUDE_SUFFIXES include INCLUDE_NAMES tbb/tbb.h
   LIBRARY_SUFFIXES lib
   COMPULSORY_COMPONENTS tbb )

# Deduce the (interface) version of TBB.
if( TBB_INCLUDE_DIR )
   file( READ "${TBB_INCLUDE_DIR}/tbb/tbb_stddef.h" _tbb_version_file )
   string( REGEX REPLACE ".*#define TBB_VERSION_MAJOR ([0-9]+).*" "\\1"
      TBB_VERSION_MAJOR "${_tbb_version_file}" )
   string( REGEX REPLACE ".*#define TBB_VERSION_MINOR ([0-9]+).*" "\\1"
      TBB_VERSION_MINOR "${_tbb_version_file}" )
   set( TBB_VERSION "${TBB_VERSION_MAJOR}.${TBB_VERSION_MINOR}" )
endif()

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( TBB
   FOUND_VAR TBB_FOUND
   REQUIRED_VARS TBB_INCLUDE_DIR TBB_INCLUDE_DIRS TBB_LIBRARIES
   VERSION_VAR TBB_VERSION )
mark_as_advanced( TBB_FOUND TBB_INCLUDE_DIR TBB_INCLUDE_DIRS
   TBB_LIBRARIES TBB_LIBRARY_DIRS )

# Set up the TBB_INSTALL_DIR directory:
get_filename_component( TBB_INSTALL_DIR ${TBB_INCLUDE_DIR} PATH CACHE )

# Set up the RPM dependency:
lcg_need_rpm( tbb )
