# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#   KIWISOLVER_PYTHON_PATH
#
# Can be steered by KIWISOLVER_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Dependencies:
find_package( PythonLibs )
find_package( PythonInterp )

# If it was already found, let's be quiet:
if( KIWISOLVER_FOUND )
   set( kiwisolver_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( KIWISOLVER_ROOT )
   set( _extraKslwArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the python path:
find_path( KIWISOLVER_PYTHON_PATH
   NAMES "kiwisolver${CMAKE_SHARED_LIBRARY_SUFFIX}"
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${KIWISOLVER_ROOT}
   ${_extraKslwArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( kiwisolver DEFAULT_MSG
   KIWISOLVER_PYTHON_PATH PYTHONLIBS_FOUND PYTHONINTERP_FOUND )

# Set up the RPM dependency:
lcg_need_rpm( kiwisolver )

# Clean up:
if( _extraKslwArgs )
   unset( _extraKslwArgs )
endif()
