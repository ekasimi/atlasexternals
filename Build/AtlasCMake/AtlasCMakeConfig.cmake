# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# This file is used to make it possible to pull in the code of this package into
# a top level CMake file using:
#
#   find_package( AtlasCMake )
#
# Then, to make CMake find this file correctly, one has to use something like
# the following on the command line:
#
#   CMAKE_PREFIX_PATH=../source/AtlasCMake cmake ../source
#
# Or:
#
#   cmake -DAtlasCMake_DIR=../source/AtlasCMake ../source
#
# The meat of the code is in the other files of the package, this file just
# provides the interface to the find_package(...) function.

# Get the current directory:
get_filename_component( _thisdir "${CMAKE_CURRENT_LIST_FILE}" PATH )
if( NOT AtlasCMake_FIND_QUIETLY )
   message( STATUS "Including AtlasCMake from directory: ${_thisdir}" )
endif()
set( ATLASCMAKE_MODULEDIR ${_thisdir}/modules CACHE STRING
   "Directory holding the ATLAS CMake files" FORCE )

# Add it to the CMake module path:
list( INSERT CMAKE_MODULE_PATH 0 ${_thisdir}/modules )
list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )

# Make sure that since this project is being included directly, the built
# projects don't shadow the modules of this package with possibly older
# versions of the files.
set( ATLAS_DONT_PREPEND_PROJECT_MODULES TRUE
   CACHE BOOL "Prevent built projects from prepending to CMAKE_MODULE_PATH" )

# Install the helper script(s) from the package:
install( PROGRAMS ${_thisdir}/scripts/acmake.py
   ${_thisdir}/scripts/cmakeNightlyInstall.sh
   ${_thisdir}/scripts/cmakeReleaseInstall.sh
   DESTINATION bin )

# If CTEST_USE_LAUNCHERS is turned on, create a dummy log file already
# during configuration, which NICOS can pick up, and conclude that the
# package's "build" was successful.
if( CTEST_USE_LAUNCHERS )
   file( MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/BuildLogs )
   file( WRITE ${CMAKE_BINARY_DIR}/BuildLogs/AtlasCMake.log
      "Dummy build log\n"
      "AtlasCMake: Package build succeeded" )
endif()

# Set ATLAS specific policies to work around temporary problems:
cmake_policy(SET CMP0058 OLD)     # suppress warning about .stamp.txt files with ninja (ATLINFR-2651)

if( ${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.12.0" )
   cmake_policy(SET CMP0074 OLD)  # suppress warning about _ROOT variables (ATLINFR-2651)
endif()

# Include the AtlasFunctions file:
include( AtlasFunctions )

# Enable/Configure the use of CTest in the project:
atlas_ctest_setup()

# Environment variable(s) needed for the build:
set( ATLASCMAKE_ENVIRONMENT
   SET GFORTRAN_UNBUFFERED_ALL "y" )

# Clean up:
unset( _thisdir )
